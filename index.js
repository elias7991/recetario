const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user');
const recipeSchema = require('./models/recipe');

const app = express();

// MongoDB connection
mongoose.connect('mongodb://elias:password@mongo:27017/recetario?authSource=admin')
  .then(() => console.log('conectado a Mongo'))
  .catch((error) => console.error(error))

// Middleware
app.use(express.json())

// Routes
// ************* POST *************
app.post('/new_user', (req, res) => {
  const user = userSchema(req.body)
  user.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})

app.post('/new_recipe', (req, res) => {
  const { userId } = req.body
  userSchema.findById(userId)
    .then((user) => {
      if (user) {
        const recipe = recipeSchema(req.body)
        recipe.save()
          .then((data) => res.json(data))
          .catch((error) => res.send(error))
      } else {
        res.status(404).json({ message: 'No se encontró registrado el usuario al que se está referenciando.' })
      }
    })
    .catch((error) => {
      console.log(error)
      res.status(500).json({ message: 'Ocurrió un error inesperado.' })
    })
})


// ************* PUT *************
app.put('/rate', (req, res) => {
  const { recipeId, userId , rating } = req.body
  userSchema.findById(userId)
    .then((user) => {
      if (user) {
        recipeSchema
          .updateOne(
              { _id: recipeId },
              [
                  {$set: {ratings: {$concatArrays: [{$ifNull: ['$ratings', []]}, [{userId: userId, rating: rating}]]}}},
                  {$set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0]}}}
              ]
          )
          .then((data) => res.json(data))
          .catch((error) => {
              console.log(error)
              res.status(500).json({ message: 'Ocurrió un error inesperado' })
          })
      } else {
        res.status(404).json({ message: 'No se encontró registrado el usuario al que se está referenciando.' })
      }
    })
    .catch((error) => {
      console.log(error)
      res.status(500).json({ message: 'Ocurrió un error inesperado' })
    })
})


// ************* GET *************
app.get('/recipes', (req, res) => {
  const { userId } = req.body
  // find recipes for user
  recipeSchema.find({ userId: userId })
    .then((recipes) => res.status(200).json(recipes))
    .catch((error) => {
      console.log(error)
      res.status(500).json({ message: 'Ocurrió un error inesperado.' })
    })
})

app.get('/recipesbyingredient', (req, res) => {
  // list of ingredients
  const ingredients = req.body.ingredients ?? []

  // Crear un array de objetos de búsqueda para cada ingrediente
  const ingredientQueries = ingredients.map((ingredient) => {
    return { 'ingredients.name': ingredient.name };
  });

  // Combinar los objetos de búsqueda usando el operador $and
  const query = { $and: ingredientQueries };

  recipeSchema.find(query)
    .then((recipes) => {
      console.log("recipes => ", recipes)
      res.json(recipes)
    })
    .catch(() => res.status(500).json({ message: 'Ocurrió un error inesperado.' }))
})

app.listen(3000, () => console.log("Escuchando el puerto 3000..."))